FROM almalinux/8-base
LABEL maintainer="Andre Staffe <andre@staffe.eu>"

ENV ZSCALER_ROOT /opt/zscaler/var/service-edge

ADD zscaler.repo /etc/yum.repos.d/
ADD start.sh /

RUN yum update -y \
    && yum install -y zpa-service-edge \
    && yum clean all \
    && systemctl enable zpa-service-edge

CMD ["/start.sh"]